# -*- coding: utf-8 -*- 

'''
Author: Steward
date: 2019-12-11 11:49 a.m.
file: planeFigure-b.py
desc: 
'''
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from mpl_toolkits.axisartist.axislines import SubplotZero
<<<<<<< HEAD:planeFigure-b.py

# fig, ax = plt.subplots()
fig = plt.figure()
ax = SubplotZero(fig, 111)
fig.add_subplot(ax)
ax.axis('equal')

for direction in ["xzero", "yzero"]:
    # adds arrows at the ends of each axis
    ax.axis[direction].set_axisline_style("-|>")

    # adds X and Y-axis from the origin
    ax.axis[direction].set_visible(True)

for direction in ["left", "right", "bottom", "top"]:
    # hides borders
    ax.axis[direction].set_visible(False)

=======

def formatAxis():
    fig = plt.figure()
    ax = SubplotZero(fig, 111)
    fig.add_subplot(ax)
    ax.axis('equal')

    for direction in ["xzero", "yzero"]:
        # adds arrows at the ends of each axis
        ax.axis[direction].set_axisline_style("-|>")

        # adds X and Y-axis from the origin
        ax.axis[direction].set_visible(True)

    for direction in ["left", "right", "bottom", "top"]:
        # hides borders
        ax.axis[direction].set_visible(False)

    return fig, ax

fig, ax = formatAxis()
    # plt.subplots()
>>>>>>> refs/remotes/origin/master:planeFigure.py

# 圆的圆心坐标
xy1 = np.array([0.2, 0.2])
# 矩形的左下角
xy2 = np.array([0.1, 0.7])
# 多边形的中心
xy3 = np.array([1.0, 0.2])
# 椭圆的中心
xy4 = np.array([1.0, 0.8])
# 多边形的中心
xy5 = np.array([0.6, 0.6])

# 第二个参数是圆的半径
circle = mpatches.Circle(xy1, 0.1, color="none")
circle.set_edgecolor(color="b")
ax.add_patch(circle)

# 第二个参数和第三个参数是先长后宽
rectangle = mpatches.Rectangle(xy2, 0.2, 0.2, color="g")
# ax.add_patch(rectangle)

# 第二个参数是边数，第三个是距离中心的位置
polygon1 = mpatches.RegularPolygon(xy3, 5, 0.1, color="none")
polygon1.set_edgecolor(color="y")
# ax.add_patch(polygon1)

# 绘制椭圆，第二个参数是椭圆的长直径，第三个是短直径
# 切记，是直径
ellipse = mpatches.Ellipse(xy4, 0.4, 0.2, color="b")
# ax.add_patch(ellipse)

polygon2 = mpatches.RegularPolygon(xy5, 6, 0.2, color="c")
# ax.add_patch(polygon2)

# 设置图形显示的时候x轴和y轴等比例
<<<<<<< HEAD:planeFigure-b.py
# ax.axis("equal")

=======
ax.axis("equal")
# formatAxis()
>>>>>>> refs/remotes/origin/master:planeFigure.py
# 添加网格
# ax.grid()

plt.show()
