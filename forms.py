#encoding: utf-8
'''
@Author：Steward 
@Time :12/10/2019 3:55 AM
@File :forms.PY
'''

# from flask_wtf import Form
from wtforms import StringField, IntegerField, TextAreaField, SubmitField, RadioField, SelectField, FloatField
from wtforms import Form, BooleanField,  PasswordField, validators
from wtforms import validators, ValidationError

class ContactForm(Form):
    name = StringField("学生姓名",[validators.data_required("Please enter your name.")])
    Gender = RadioField('性别', choices = [('M','Male'),('F','Female')])
    Address = TextAreaField("地址")

    email = StringField("Email",[validators.required("Please enter your email address."),
      validators.Email("Please enter your valid email address.")])

    Age = IntegerField("年龄")
    language = SelectField('语言', choices = [('cpp', 'C++'), ('py', 'Python')])
    submit = SubmitField("提交")



class RegistrationForm(Form):
    username = StringField('Username', [validators.Length(min=4, max=25)])
    email = StringField('Email Address', [validators.Length(min=6, max=35),
                                          validators.Email("Please enter your valid email address.")])
    password = PasswordField('New Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    accept_tos = BooleanField('I accept the TOS', [validators.DataRequired()])


class CurveForm(Form):
    a = FloatField('a', [validators.number_range(min=-5.0, max=5.0)])
    b = FloatField('b', [validators.number_range(min=-5.0, max=5.0)])
    c = FloatField('c', [validators.number_range(min=-5.0, max=5.0)])

class StraightlinrForm(Form):
    a = FloatField('a', [validators.number_range(min=-5.0, max=5.0)])
    b = FloatField('b', [validators.number_range(min=-5.0, max=5.0)])

class LineForm(Form):
    a = FloatField('a', [validators.number_range(min=-1.0, max=1.0)])
    b = FloatField('b', [validators.number_range(min=-5.0, max=5.0)])
    c = FloatField('c', [validators.number_range(min=-5.0, max=5.0)])
    k = FloatField('k', [validators.number_range(min=-5.0, max=5.0)])
    m = FloatField('m', [validators.number_range(min=-5.0, max=5.0)])

class CircleForm(Form):
    a = FloatField('a', [validators.number_range(min=-5.0, max=5.0)])
    b = FloatField('b', [validators.number_range(min=-5.0, max=5.0)])
    r = FloatField('r', [validators.number_range(min=-5.0, max=5.0)])

class EllipseForm(Form):
    a = FloatField('a', [validators.number_range(min=-3.0, max=3.0)])
    b = FloatField('b', [validators.number_range(min=-3.0, max=3.0)])
    h = FloatField('h', [validators.number_range(min=-2.0, max=2.0)])
    k = FloatField('k', [validators.number_range(min=-2.0, max=2.0)])

class TriangleForm(Form):
    x1 = FloatField('x1', [validators.number_range(min=-5.0, max=5.0)])
    y1 = FloatField('y1', [validators.number_range(min=-5.0, max=5.0)])
    x2 = FloatField('x2', [validators.number_range(min=-5.0, max=5.0)])
    y2 = FloatField('y2', [validators.number_range(min=-5.0, max=5.0)])
    x3 = FloatField('x3', [validators.number_range(min=-5.0, max=5.0)])
    y3 = FloatField('y3', [validators.number_range(min=-5.0, max=5.0)])

class SquareForm(Form):
    x1 = FloatField('x1', [validators.number_range(min=-5.0, max=5.0)])
    y1 = FloatField('y1', [validators.number_range(min=-5.0, max=5.0)])
    x2 = FloatField('x2', [validators.number_range(min=-5.0, max=5.0)])
    y2 = FloatField('y2', [validators.number_range(min=-5.0, max=5.0)])

class SphereForm(Form):
    x = FloatField('x', [validators.number_range(min=-5.0, max=5.0)])
    y = FloatField('y', [validators.number_range(min=-5.0, max=5.0)])
    z = FloatField('z', [validators.number_range(min=-5.0, max=5.0)])

class CylinderForm(Form):
    r = FloatField('r', [validators.number_range(min=-5.0, max=5.0)])
    h = FloatField('h', [validators.number_range(min=-5.0, max=5.0)])

class ConeForm(Form):
    r = FloatField('r', [validators.number_range(min=-5.0, max=5.0)])
    h = FloatField('h', [validators.number_range(min=-5.0, max=5.0)])