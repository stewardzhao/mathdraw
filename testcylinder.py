# -*- coding: utf-8 -*- 

'''
Author: Steward
date: 2019-12-16 10:27 a.m.
file: testcylinder.py
desc: 
'''
from flask import Flask, render_template, request, flash, url_for

import base64
from io import BytesIO
from math import pi, cos, sin

import matplotlib.pyplot as plt
import numpy as np
import plotly.graph_objects as go

from matplotlib.patches import PathPatch
import matplotlib.patches as mpatches
from matplotlib.path import Path
from mpl_toolkits.axisartist.axislines import SubplotZero
from mpl_toolkits.mplot3d import Axes3D
# app = Flask(__name__)


def cylinder(r, h, a=0, nt=100, nv=50):
    """
    parametrize the cylinder of radius r, height h, base point a
    """
    theta = np.linspace(0, 2 * np.pi, nt)
    v = np.linspace(a, a + h, nv)
    theta, v = np.meshgrid(theta, v)
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    z = v
    return x, y, z

# @app.route('/')
# def index():
fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')

z = np.arange(0, 2, 0.02)
theta = np.arange(0, 3 * pi + pi / 50, pi / 50)
ax = fig.add_subplot(111, projection='3d')
for zval in z:
    x = zval * np.array([cos(q) for q in theta])
    y = zval * np.array([sin(q) for q in theta])
    ax.plot(x, y, -zval, 'b-')

plt.tight_layout()
plt.show()

# if __name__ == '__main__':
#     app.run()