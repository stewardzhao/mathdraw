# encoding:utf8
from flask import Flask, render_template, request, flash, url_for

import base64
from io import BytesIO
from math import pi, cos, sin

import matplotlib.pyplot as plt
import numpy as np
import plotly.graph_objects as go

from matplotlib.patches import PathPatch
import matplotlib.patches as mpatches
from matplotlib.path import Path
from mpl_toolkits.axisartist.axislines import SubplotZero
from mpl_toolkits.mplot3d import Axes3D


from forms import *
    # CurveForm, StraightlinrForm, CircleForm, EllipseForm, TriangleForm, SquareForm

app = Flask(__name__)
app.secret_key = "don't guess the key"




# format axis add x,y axis arrows
def formatAxis():
    fig = plt.figure()
    ax = SubplotZero(fig, 111)
    fig.add_subplot(ax)
    ax.axis('equal')

    for direction in ["xzero", "yzero"]:
        # adds arrows at the ends of each axis
        ax.axis[direction].set_axisline_style("-|>")

        # adds X and Y-axis from the origin
        ax.axis[direction].set_visible(True)

    for direction in ["left", "right", "bottom", "top"]:
        # hides borders
        ax.axis[direction].set_visible(False)

    return fig, ax

# convert imgage data
def FormatImgdata(plt):
    # 转成图片的步骤
    sio = BytesIO()
    plt.savefig(sio, format='png')
    data = base64.encodebytes(sio.getvalue()).decode()
    src = 'data:image/png;base64,' + str(data)
    sio = ''

    # 记得关闭，不然画出来的图是重复的
    plt.close()
    return src


@app.route('/')
def base():
    return render_template('base.html')


@app.route('/sphere/', methods=['GET', 'POST'])
def sphere():
    form = SphereForm(request.form)
    if request.method == 'POST' and form.validate():
        x = form.x.data
        y = form.y.data
        z = form.z.data

        expression = 'Sphere coordinate: \\( X = ' + \
            str(x) + ' \\; Y= ' + str(y) + '\\;Z = ' + str(z) + '\\)'


        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        # Make data
        u = np.linspace(0, 2 * np.pi, 100)
        v = np.linspace(0, np.pi, 100)
        x0 = x * np.outer(np.cos(u), np.sin(v))
        y0 = y * np.outer(np.sin(u), np.sin(v))
        z0 = z * np.outer(np.ones(np.size(u)), np.cos(v))

        # Plot the surface
        ax.plot_surface(x0, y0, z0, color='b')

        # plot the wireframe
        ax.plot_wireframe(x0, y0, z0)

        plt.tight_layout()

        src = FormatImgdata(plt)

        params = {
            'form': form,
            'expression': expression,
            'src': src,
        }
        return render_template("sphere.html", **params)
    return render_template('sphere.html', form=form)


@app.route('/cylinder/', methods=['GET', 'POST'])
def cylinder():
    form = CylinderForm(request.form)
    if request.method == 'POST' and form.validate():
        r = form.r.data
        h = form.h.data

        expression = 'Cylinder Parameters: \\((Radius = ' + \
            str(r) + ', Height = ' + str(h) + ')\\)'

        # def cylinder(r, h, a=0, nt=100, nv=50):
        #     """
        #     parametrize the cylinder of radius r, height h, base point a
        #     """
        #     theta = np.linspace(0, 2 * np.pi, nt)
        #     v = np.linspace(a, a + h, nv)
        #     theta, v = np.meshgrid(theta, v)
        #     x = r * np.cos(theta)
        #     y = r * np.sin(theta)
        #     z = v
        #     return x, y, z

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        # x, y, z = cylinder(r, h)
        theta = np.linspace(0, 2 * np.pi, 100)
        v = np.linspace(0, h, 50)
        theta, v = np.meshgrid(theta, v)
        x = r * np.cos(theta)
        y = r * np.sin(theta)
        z = v
        # Plot the surface
        ax.plot_surface(x, y, z, color='b')

        # plot the wireframe
        ax.plot_wireframe(x, y, z)

        plt.tight_layout()
        # plt.show()
        src = FormatImgdata(plt)

        params = {
            'form': form,
            'expression': expression,
            'src': src,
        }
        return render_template("cylinder.html", **params)

    return render_template('cylinder.html', form=form)


@app.route('/cone/', methods=['GET', 'POST'])
def cone():
    form = ConeForm(request.form)
    if request.method == 'POST' and form.validate():
        r = form.r.data
        h = form.h.data

        expression = 'Cone Parameters: \\((Radius = ' + \
                     str(r) + ', Height = ' + str(h) + ')\\)'

        z = np.arange(0, h, 0.02)
        theta = np.arange(0, r * pi + pi / 50, pi / 50)

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        for zval in z:
            x = zval * np.array([cos(q) for q in theta])
            y = zval * np.array([sin(q) for q in theta])
            ax.plot(x, y, -zval, 'b-')

        # if request.method == 'GET':
    #     return render_template('cone.html',)
    # else:
    #     a = request.form.get('coordinate1')
    #     b = request.form.get('coordinate2')
    #     c = request.form.get('coordinate3')
    #     d = request.form.get('coordinate4')
    #
    #     expression = 'Cone Parameters: \\((x=[' + a + '],y=[' + b + '],z=[' + c + \
    #         '],u=[0],v=[0],w=[' + d + '])\\) camera_eye=dict(\\(x=-1.57, y=1.36, z=0.58) \\)'
    #     # annotate_str = expression
    #     x = float(a)
    #     y = float(b)
    #     z = float(c)
    #     w = float(d)
    #
    #     fig = go.Figure(data=go.Cone(
    #         x=[x],
    #         y=[y],
    #         z=[z],
    #         u=[0],
    #         v=[0],
    #         w=[w],
    #         sizemode="absolute",
    #         sizeref=3,
    #         anchor="tip"))
    #
    #     fig.update_layout(
    #         scene=dict(domain_x=[1, 1],
    #                    camera_eye=dict(x=-1.57, y=1.36, z=0.58)))
    #
    #     # fig.show()
    #
    #     sio = BytesIO()
    #     plt.savefig(sio, format='png')
    #     data = base64.encodebytes(sio.getvalue()).decode()
    #     src = 'data:image/png;base64,' + str(data)
    #     # fig.show()
    #     # 记得关闭，不然画出来的图是重复的
    #     # plt.close()
        src = FormatImgdata(plt)
        params = {
            'form': form,
            'expression': expression,
            'src': src,
        }


        return render_template('cone.html', **params)
    return render_template('cone.html', form=form)

@app.route('/straightline/', methods=['GET', 'POST'])
def straightline():
    form = StraightlinrForm(request.form)
    if request.method == 'POST' and form.validate():
        a = form.a.data
        b = form.b.data

        if a == 0:
            if b == 0:
                expression = '\\(y = 0\\)'
            else:
                expression = '\\(y = ' + str(b) +'\\)'
        else:
            if b == 0:
                expression = '\\(y = ' + str(a) + 'x \\)'
            else:
                if b < 0:
                    expression = '\\(y = ' + str(a) + 'x ' + str(b) + '\\)'
                else:
                    expression = '\\(y = ' + str(a) + 'x +' + str(b) + '\\)'
        # expression = '\\(y = ' + str(a) + 'x^2 +' + str(b) + 'x +' + str(c) + '\\)'
        flash('Thanks for drawing straightline: ' + expression)
        x = np.linspace(-10, 10, 200)
        y = (a * x) + b

        formatAxis()  # figsize=(8, 5))
        plt.plot(x, y)

        plt.tight_layout()

        src = FormatImgdata(plt)
        # plt.close()
        # 记得关闭，不然画出来的图是重复的

        params = {
            'form': form,
            'expression': expression,
            'src': src,
        }
        return render_template('straightline.html', **params)
    return render_template('straightline.html', form=form)

@app.route('/curve/', methods=['GET', 'POST'])
def curve():
    form = CurveForm(request.form)
    if request.method == 'POST' and form.validate():
        a = form.a.data
        b = form.b.data
        c = form.c.data

        if a == 0:
            if b == 0:
                expression = '\\(y = ' + str(c) + '\\)'
            elif c == 0:
                expression = '\\(y = ' + str(b) + 'x \\)'
            elif c < 0:
                expression = '\\(y = ' + str(b) + 'x ' + str(c) + '\\)'
            else:
                expression = '\\(y = ' + str(b) + 'x +' + str(c) + '\\)'
        else:
            if b == 0:
                if c < 0:
                    expression = '\\(y = ' + str(a) + 'x^2 ' + str(c) + '\\)'
                elif c == 0:
                    expression = '\\(y = ' + str(a) + 'x^2 \\)'
                else:
                    expression = '\\(y = ' + str(a) + 'x^2 +' + str(c) + '\\)'
            elif c == 0:
                if b < 0:
                    expression = '\\(y = ' + str(a) + 'x^2 ' + str(b) + 'x \\)'
                else:
                    expression = '\\(y = ' + str(a) + 'x^2 +' + str(b) + 'x \\)'
            elif c < 0:
                if b < 0:
                    expression = '\\(y = ' + str(a) + 'x^2 ' + str(b) + 'x ' + str(c) + '\\)'
                else:
                    expression = '\\(y = ' + str(a) + 'x^2 +' + str(b) + 'x ' + str(c) + '\\)'
            else:
                if b < 0:
                    expression = '\\(y = ' + str(a) + 'x^2 ' + \
                                 str(b) + 'x +' + str(c) + '\\)'
                else:
                    expression = '\\(y = ' + str(a) + 'x^2 +' + \
                    str(b) + 'x +' + str(c) + '\\)'
        # expression = '\\(y = ' + str(a) + 'x^2 +' + str(b) + 'x +' + str(c) + '\\)'
        flash('Thanks for drawing curve: '+ expression)
        x = np.linspace(-10, 10, 200)
        y = (a * x * x) + (b * x) + c

        formatAxis()  # figsize=(8, 5))
        plt.plot(x, y)

        plt.tight_layout()

        src = FormatImgdata(plt)
        # plt.close()
        # 记得关闭，不然画出来的图是重复的

        params = {
            'form': form,
            'expression': expression,
            'src': src,
        }
        return render_template('curve.html', **params)
    return render_template('curve.html', form=form)

@app.route('/line/', methods=['GET', 'POST'])
def line():
    form = LineForm(request.form)
    if request.method == 'POST' and form.validate():
        a = form.a.data
        b = form.b.data
        c = form.c.data
        k = form.k.data
        m = form.m.data

        if a == 0:
            if b == 0:
                expression = '\\(y = ' + str(c) + '\\)'
            elif c == 0:
                expression = '\\(y = ' + str(b) + 'x \\)'
            elif c < 0:
                expression = '\\(y = ' + str(b) + 'x ' + str(c) + '\\)'
            else:
                expression = '\\(y = ' + str(b) + 'x +' + str(c) + '\\)'
        else:
            if b == 0:
                if c < 0:
                    expression = '\\(y = ' + str(a) + 'x^2 ' + str(c) + '\\)'
                elif c == 0:
                    expression = '\\(y = ' + str(a) + 'x^2 \\)'
                else:
                    expression = '\\(y = ' + str(a) + 'x^2 +' + str(c) + '\\)'
            elif c == 0:
                if b < 0:
                    expression = '\\(y = ' + str(a) + 'x^2 ' + str(b) + 'x \\)'
                else:
                    expression = '\\(y = ' + str(a) + 'x^2 +' + str(b) + 'x \\)'
            elif c < 0:
                if b < 0:
                    expression = '\\(y = ' + str(a) + 'x^2 ' + str(b) + 'x ' + str(c) + '\\)'
                else:
                    expression = '\\(y = ' + str(a) + 'x^2 +' + str(b) + 'x ' + str(c) + '\\)'
            else:
                if b < 0:
                    expression = '\\(y = ' + str(a) + 'x^2 ' + \
                                 str(b) + 'x +' + str(c) + '\\)'
                else:
                    expression = '\\(y = ' + str(a) + 'x^2 +' + \
                    str(b) + 'x +' + str(c) + '\\)'

        expression_curve = expression
        expression_straightline = '\\(y = ' +  str(k) + 'x +' + str(m) + '\\)'
        # flash('Thanks for drawing curve: '+ expression)
        x = np.linspace(-10, 10, 200)
        y = (a * x * x) + (b * x) + c
        y1 = (k * x) + m

        formatAxis()  # figsize=(8, 5))
        plt.plot(x, y)
        plt.plot(x, y1)

        # import matplotlib.pyplot as plt
        # plt.rcParams["figsize"] = (8, 5)
        plt.rcParams["figure.figsize"] = (8, 5)

        plt.tight_layout()

        src = FormatImgdata(plt)
        # plt.close()
        # 记得关闭，不然画出来的图是重复的

        params = {
            'form': form,
            'expression_curve': expression_curve,
            'expression_straightline': expression_straightline,
            'src': src,
        }
        return render_template('line.html', **params)

    params = {
        'form': form,
        'expression_curve': '\\(y = ax^2 + bx + c\\)',
        'expression_straightline': '\\(y = kx + m\\)',
    }
    return render_template('line.html', **params)

@app.route('/circle/', methods=['GET', 'POST'])
def circle():
    form = CircleForm(request.form)
    if request.method == 'POST' and form.validate():
        a = form.a.data
        b = form.b.data
        r = form.r.data

        expression = '\\((x - ' + str(a) + ') ^ 2 + (y - ' + \
                     str(b) + ') ^ 2 = ' + str(r) + ' ^ 2\\)'
        annotate_str = 'Center(' + str(a) + ',' + str(b) + ') Radius = ' + str(r)
        flash('Thanks for drawing circle: ' + expression)

        fig, ax = formatAxis()
        # 第二个参数是圆的半径
        circle1 = mpatches.Circle(np.array([a, b]), r, color="none")
        circle1.set_edgecolor(color="b")
        ax.add_patch(circle1)
        # plt.plot(x, y)

        plt.plot([a, a, ], [b, (b + r), ], 'k--', linewidth=1.5)
        plt.scatter([a, a], [b, (b + r)], s=50, color='b')

        plt.annotate(r'$%s$' % annotate_str, xy=(a, b), xycoords='data', xytext=(+20, -20),
                     textcoords='offset points', fontsize=11,
                     arrowprops=dict(arrowstyle='->', connectionstyle="arc3,rad=.2"))

        plt.tight_layout()

        src = FormatImgdata(plt)

        params = {
            'form': form,
            'expression': expression,
            'src': src,
        }
        return render_template('circle.html', **params)
    return render_template('circle.html', form=form)

@app.route('/ellipse/', methods=['GET', 'POST'])
def ellipse():
    form = EllipseForm(request.form)
    if request.method == 'POST' and form.validate():
        a = form.a.data
        b = form.b.data
        h = form.h.data
        k = form.k.data
        

        expression = '\\(\\frac{(x-' + str(h) + ')^2}{' + str(a) + \
                     '^2}+\\frac{(y-' + str(k) + ')^2}{' + str(b) + '^2}=1\\)'
        flash('Thanks for drawing circle: ' + expression)

        t_rot = pi / 4  # rotation angle

        t = np.linspace(0, 2 * pi, 100)
        Ell = np.array([a * np.cos(t), b * np.sin(t)])
        # u,v removed to keep the same center location
        R_rot = np.array([[cos(t_rot), -sin(t_rot)], [sin(t_rot), cos(t_rot)]])
        # 2-D rotation matrix

        Ell_rot = np.zeros((2, Ell.shape[1]))
        for i in range(Ell.shape[1]):
            Ell_rot[:, i] = np.dot(R_rot, Ell[:, i])

        formatAxis()  # figsize=(8, 5))
        plt.plot(h + Ell[0, :], k + Ell[1, :])  # initial ellipse
        plt.plot(h + Ell_rot[0, :], k + Ell_rot[1, :],
                 'darkorange')  # rotated ellipse
        plt.grid(color='lightgray', linestyle='--')

        plt.tight_layout()

        src = FormatImgdata(plt)
        # plt.close()
        # 记得关闭，不然画出来的图是重复的

        params = {
            'form': form,
            'expression': expression,
            'src': src,
        }
        return render_template('ellipse.html', **params)
    return render_template('ellipse.html', form=form)

@app.route('/triangle/', methods=['GET', 'POST'])
def triangle():
    form = TriangleForm(request.form)
    if request.method == 'POST' and form.validate():
        x1 = form.x1.data
        y1 = form.y1.data
        x2 = form.x2.data
        y2 = form.y2.data
        x3 = form.x3.data
        y3 = form.y3.data


        expression = '\\(Coordinates 1 (' + str(x1) + ', ' + str(y1) + '),Coordinates 2 (' + \
            str(x2) + ', ' + str(y2) + '),Coordinates 3 (' + str(x3) + ', ' + str(y3) + ')\\)'
        # annotate_str = expression

        vertices = []
        codes = []

        # codes = [Path.MOVETO] + [Path.LINETO] * 3 + [Path.CLOSEPOLY]
        # vertices = [(1, 1), (1, 2), (2, 2), (2, 1), (0, 0)]

        codes += [Path.MOVETO] + [Path.LINETO] * 2 + [Path.CLOSEPOLY]
        vertices += [(x1, y1), (x2, y2), (x3, y3), (0, 0)]

        vertices = np.array(vertices, float)
        path = Path(vertices, codes)

        pathpatch = PathPatch(path, facecolor='None', edgecolor='green')

        # plt =     formatAxis()  # figsize=(8, 5))
        # plt.plot(x, y)
        # fig, ax = plt.subplots()

        # kk
        fig, ax = formatAxis()
        # ax = SubplotZero(fig, 111)
        # fig.add_subplot(ax)
        #
        # for direction in ["xzero", "yzero"]:
        #     # adds arrows at the ends of each axis
        #     ax.axis[direction].set_axisline_style("-|>")
        #
        #     # adds X and Y-axis from the origin
        #     ax.axis[direction].set_visible(True)
        #
        # for direction in ["left", "right", "bottom", "top"]:
        #     # hides borders
        #     ax.axis[direction].set_visible(False)

        ax.add_patch(pathpatch)
        # ax.set_title('A compound path')

        ax.dataLim.update_from_data_xy(vertices)
        ax.autoscale_view()
        # plt.show()
        # ax.axis('equal')
        # ax.spines['right'].set_color('none')
        # ax.spines['top'].set_color('none')
        # ax.xaxis.set_ticks_position('bottom')  # set bottom border is X
        # # ax.yaxis.set_ticks_position('left')  # set left border is Y
        # ax.spines['bottom'].set_position(('data', 0))  # move Y to 0
        # ax.spines['left'].set_position(('data', 0))
        #   formatAxis(plt)
        src = FormatImgdata(plt)
        # plt.close()
        # 记得关闭，不然画出来的图是重复的

        params = {
            'form': form,
            'expression': expression,
            'src': src,
        }
        return render_template("triangle.html", **params)
    return render_template("triangle.html", form=form)

@app.route('/square/', methods=['GET', 'POST'])
def square():
    form = SquareForm(request.form)
    if request.method == 'POST' and form.validate():
        x1 = form.x1.data
        y1 = form.y1.data
        x2 = form.x2.data
        y2 = form.y2.data

        expression = '\\(Coordinates 1 (' + str(x1) + ', ' + str(y1) + \
            '),Coordinates 2 (' + str(x2) + ', ' + str(y2) + ')\\)'

        vertices = []
        codes = []

        codes = [Path.MOVETO] + [Path.LINETO] * 3 + [Path.CLOSEPOLY]
        vertices = [(x1, y1), (x1, y2), (x2, y2), (x2, y1), (0, 0)]

        vertices = np.array(vertices, float)
        path = Path(vertices, codes)

        pathpatch = PathPatch(path, facecolor='None', edgecolor='green')

        # fig, ax = plt.subplots()
        fig, ax = formatAxis()
        ax.add_patch(pathpatch)

        ax.dataLim.update_from_data_xy(vertices)
        ax.autoscale_view()

        src = FormatImgdata(plt)

        params = {
            'form': form,
            'expression': expression,
            'src': src,
        }
        return render_template("square.html", **params)
    return render_template('square.html', form=form)


if __name__ == '__main__':
    app.run(debug=True)
